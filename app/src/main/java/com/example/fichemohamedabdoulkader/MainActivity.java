package com.example.fichemohamedabdoulkader;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    // Variables pour les composants de la première activité
    private TextView firstName, lastName, phoneNumber;
    private Button editIdentityButton, editAdresse; // Boutons pour l'édition d'identité et d'adresse
    private ActivityResultLauncher<Intent> secondActivityLauncher; // Lanceur d'activité pour la deuxième activité

    // Variables pour les composants de la troisième activité
    private TextView num, rue, postale, ville;

    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Initialisation des composants de la première activité
        firstName = findViewById(R.id.prenomm_data);
        lastName = findViewById(R.id.nom_data);
        phoneNumber = findViewById(R.id.tel_data);

        // Initialisation des composants de la troisième activité
        num = findViewById(R.id.numero_data);
        rue = findViewById(R.id.rue_data);
        postale = findViewById(R.id.postale_data);
        ville = findViewById(R.id.ville_data);

        // Initialisation des boutons
        editIdentityButton = findViewById(R.id.Modifier_Identite);
        editAdresse = findViewById(R.id.modifie_adresse);

        // Initialisation du lanceur d'activité
        secondActivityLauncher = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                result -> {
                    Intent data = result.getData();
                    if (data != null) {
                        switch (result.getResultCode()){
                            case RESULT_OK:
                                // Données reçues de la deuxième activité
                                String newFirstName = data.getStringExtra("newFirstName");
                                String newNom = data.getStringExtra("newNom");
                                String newPhoneNumber = data.getStringExtra("newPhoneNumber");

                                // Mettre à jour les TextView avec les nouvelles données
                                firstName.setText(newFirstName);
                                lastName.setText(newNom);
                                phoneNumber.setText(newPhoneNumber);
                                break;
                            case 5:
                                // Données reçues de la troisième activité
                                String numero1 = data.getStringExtra("numero");
                                String rue1 = data.getStringExtra("rue");
                                String postale1 = data.getStringExtra("postale");
                                String ville1 = data.getStringExtra("ville");

                                // Mettre à jour les TextView avec les nouvelles données
                                num.setText(numero1);
                                rue.setText(rue1);
                                postale.setText(postale1);
                                ville.setText(ville1);
                        }
                    }
                });

        // Gestion du clic sur le bouton pour l'édition d'identité
        editIdentityButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, SecondActivity.class);
                intent.putExtra("newFirstName", firstName.getText().toString());
                intent.putExtra("newNom", lastName.getText().toString());
                intent.putExtra("newPhoneNumber", phoneNumber.getText().toString());
                secondActivityLauncher.launch(intent);
            }
        });

        // Gestion du clic sur le bouton pour l'édition d'adresse
        editAdresse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ThreeActivity.class);
                intent.putExtra("numero", num.getText().toString());
                intent.putExtra("rue", rue.getText().toString());
                intent.putExtra("postale", postale.getText().toString());
                intent.putExtra("ville", postale.getText().toString());
                secondActivityLauncher.launch(intent);
            }
        });
    }
}