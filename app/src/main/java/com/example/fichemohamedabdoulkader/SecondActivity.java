package com.example.fichemohamedabdoulkader;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class SecondActivity extends AppCompatActivity {
    private EditText editTextFirstName, editTextNom, editTextPhoneNumber;
    private Button buttonOk, buttonCancel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        editTextFirstName = findViewById(R.id.editNom1);
        editTextNom = findViewById(R.id.editPrenom);
        editTextPhoneNumber = findViewById(R.id.editTel);

        buttonOk = findViewById(R.id.okBtn);
        buttonCancel = findViewById(R.id.cancelBtn);

        String initialFirstName = getIntent().getStringExtra("newFirstName");
        String initialNom = getIntent().getStringExtra("newNom");
        String initialPhoneNumber = getIntent().getStringExtra("newPhoneNumber");

        if (!initialFirstName.equals("Inconnu")) {
            editTextFirstName.setText(initialFirstName);
        }
        if (!initialNom.equals("Inconnu")) {
            editTextNom.setText(initialNom);
        }
        if (!initialPhoneNumber.equals("Inconnu")) {
            editTextPhoneNumber.setText(initialPhoneNumber);
        }

        buttonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String newFirstName = editTextFirstName.getText().toString();
                String newNom = editTextNom.getText().toString();
                String newPhoneNumber = editTextPhoneNumber.getText().toString();

                Intent intent = new Intent();
                intent.putExtra("newFirstName", newFirstName);
                intent.putExtra("newNom", newNom);
                intent.putExtra("newPhoneNumber", newPhoneNumber);

                setResult(RESULT_OK, intent);
                finish();
            }
        });

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });
    }
    }
