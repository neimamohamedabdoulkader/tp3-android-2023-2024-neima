package com.example.fichemohamedabdoulkader;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class ThreeActivity extends AppCompatActivity {

    private EditText editTextNumber, editTextStreet, editTextPostalCode, editTextCity;
    private Button buttonOk, buttonCancel;

    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_three);
        editTextNumber = findViewById(R.id.editTextnum);
        editTextStreet = findViewById(R.id.editTextRue);
        editTextPostalCode = findViewById(R.id.editTextpostale);
        editTextCity = findViewById(R.id.editTextVille);

        buttonOk = findViewById(R.id.Ok2);
        buttonCancel = findViewById(R.id.cancel2);

        String num = getIntent().getStringExtra("numero");
        String rue = getIntent().getStringExtra("rue");
        String postal = getIntent().getStringExtra("postale");
        String ville = getIntent().getStringExtra("ville");

        if (!num.equals("Inconnu")) {
            editTextNumber.setText(num);
        }
        if (!rue.equals("Inconnu")) {
            editTextStreet.setText(rue);
        }
        if (!postal.equals("Inconnu")) {
            editTextPostalCode.setText(postal);
        }
        if (!ville.equals("Inconnu")) {
            editTextCity.setText(ville);
        }

        buttonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nu = editTextNumber.getText().toString();
                String ru = editTextStreet.getText().toString();
                String pos = editTextPostalCode.getText().toString();
                String vil = editTextCity.getText().toString();

                Intent intent = new Intent();
                intent.putExtra("numero", nu);
                intent.putExtra("rue", ru);
                intent.putExtra("postale", pos);
                intent.putExtra("ville", vil);

                setResult(5, intent);
                finish();
            }
        });

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });
    }
}